[![Build Status](https://travis-ci.org/roldaojr/BescheLab.svg?branch=master)](https://travis-ci.org/roldaojr/BescheLab)

# BescheLab

Sistema de gestão de laboratórios clínicos

## Instalando

Instalando requisitos

    pip install -r requirements.txt

Criando o banco de dados

    python manage.py migrate

Criar uma usuário inicial

    python manage.py createsuperuser

Cadastrar Exames

    python manage.py atualizar_exames
    
Executando servidor de testes

    python manage.py runserver
