# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-10 03:11
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('financeiro', '0002_auto_20170207_2149'),
    ]

    operations = [
        migrations.AlterField(
            model_name='lancamento',
            name='n_parcelas',
            field=models.IntegerField(blank=True, default=0, null=True),
        ),
    ]
