# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import migrations


def remove_old_permissions(apps, schema_editor):
    ContentType = apps.get_model('contenttypes', 'ContentType')
    try:
        movimento_ct = ContentType.objects.get(app_label='financeiro',
                                               model='movimento')
        Permission = apps.get_model('auth', 'permission')
        Permission.objects.filter(content_type=movimento_ct).delete()
    except ContentType.DoesNotExist:
        pass


class Migration(migrations.Migration):

    dependencies = [
        ('financeiro', '0012_auto_20170303_1104'),
    ]

    operations = [
        migrations.RunPython(remove_old_permissions)
    ]
