# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-19 20:18
from __future__ import unicode_literals
from django.db import migrations, models


def adicionar_vencimentos(apps, *args, **kwargs):
    Movimento = apps.get_model('financeiro', 'Movimento')
    for m in Movimento.objects.all():
        if m.pago:
            m.data_pagamento = m.data
            m.save()


def remover_vencimentos(*args, **kwargs):
    pass


class Migration(migrations.Migration):

    dependencies = [
        ('financeiro', '0016_auto_20170316_1055'),
    ]

    operations = [
        migrations.AddField(
            model_name='movimento',
            name='data_pagamento',
            field=models.DateField(null=True),
        ),
        migrations.RunPython(adicionar_vencimentos, remover_vencimentos)
    ]
