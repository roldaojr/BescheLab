# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('financeiro', '0010_auto_20170303_1040'),
    ]

    operations = [
        migrations.RenameModel("Pagamento", 'Movimento')
    ]
