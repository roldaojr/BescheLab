# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-05-08 08:23
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('financeiro', '0024_auto_20170508_0457'),
    ]

    operations = [
        migrations.RenameField(
            model_name='lancamento',
            old_name='pago',
            new_name='pagoL',
        ),
    ]
