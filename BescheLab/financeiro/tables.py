# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from table import Table
from table.columns import Column
from table.columns.linkcolumn import LinkColumn, Link
from table.utils import A
from ..layout.tables import (NumberColumn, DateColumn, BooleanColumn,
                             TextColumn)


class LancamentoTable(Table):
    data = DateColumn(field='data', header='Lançamento')
    descricao = TextColumn(field='descricao', header='Descrição')
    pessoa = Column(field='pessoa', header='Pessoa')
    parcelado = BooleanColumn(field='parcelado', header='Parcelado')
    valor = NumberColumn(field='valor', header='Valor')

    class Meta:
        attrs = {
            'class': 'table-striped dt-responsive nowrap'
        }


class ListarLancamentoTable(Table):
    pessoa = LinkColumn(field='pessoa.nome', header='Pessoa', links=[
        Link(viewname='lancamento_detalhar', args=(A('pk'),),
             text=A('pessoa.nome'))])
    descricao = TextColumn(field='descricao', header='Descrição')
    valor = NumberColumn(field='valor', header='Valor')
    parcelado = BooleanColumn(field='parcelado', header='Parcelado')
    data = DateColumn(field='data', header='Data')

    class Meta:
        attrs = {
            'class': 'table-striped dt-responsive nowrap rowlink',
            'data-link': 'row'
        }


class MovimentoTable(Table):
    tipo = Column(field='lancamento.tipo', header='Tipo')
    nome = Column(field='lancamento.pessoa.nome', header='Nome')
    valor = NumberColumn(field='valor', header='Valor')
    data_pago = DateColumn(field='data_pagamento', header='Data do pagamento')

    class Meta:
        attrs = {
            'class': 'table-striped dt-responsive nowrap'
        }
