# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from datetime import timedelta
from django.utils.encoding import python_2_unicode_compatible
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.timezone import now
from djchoices import DjangoChoices, ChoiceItem
from auditlog.registry import auditlog
from ..pontos_coleta.models import PontoColeta
from ..pessoa.models import Pessoa


@python_2_unicode_compatible
class Lancamento(models.Model):
    class Tipos(DjangoChoices):
        Receita = ChoiceItem(1, "Receita")
        Receita.icon = 'fa fa-level-up'
        Despesa = ChoiceItem(2, 'Despesa')
        Despesa.icon = 'fa fa-level-down'

    ponto_coleta = models.ForeignKey(PontoColeta, editable=False)
    pessoa = models.ForeignKey(Pessoa)
    tipo = models.IntegerField(choices=Tipos.choices, default=Tipos.Receita,
                               editable=False)
    descricao = models.TextField('descrição')
    valor = models.DecimalField(max_digits=10, decimal_places=2)
    entrada = models.DecimalField(max_digits=10, decimal_places=2, default=0, null=True, blank=True)
    data = models.DateField(default=now)
    n_parcelas = models.IntegerField('Parcelas', default=0)
    pago = models.BooleanField(default=False)

    def __str__(self):
        return 'R$ %s (%s) Entrada %s' % (self.valor, self.descricao, self.entrada)

    @property
    def parcelado(self):
        return self.n_parcelas > 0

    class Meta:
        verbose_name = 'Lançamento'
        default_permissions = []
        permissions = (
            ('add_lancamento', 'Adicionar'),
            ('delete_lancamento', 'Excluir'),
            ('relatorio_lancamento', 'Relatório'),
        )


@python_2_unicode_compatible
class Movimento(models.Model):
    lancamento = models.ForeignKey(Lancamento, on_delete=models.CASCADE,
                                   related_name='movimentos')
    data = models.DateField(default=now)
    parcela = models.IntegerField('Nº da parcela', default=0)
    valor = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    pago = models.BooleanField(default=False)
    data_pagamento = models.DateField(null=True)

    def save(self, *args, **kwargs):
        result = super(Movimento, self).save(*args, **kwargs)
        movimentos = Movimento.objects.filter(lancamento=self.lancamento)
        if False not in [m.pago for m in movimentos]:
            self.lancamento.pago = True
            self.lancamento.save()
        return result

    def __str__(self):
        if self.parcela:
            return 'Parcela %s, R$ %s' % (self.parcela, self.valor)
        else:
            return '%s' % self.valor

    class Meta:
        verbose_name = 'Movimento'
        default_permissions = []
        permissions = (
            ('relatorio_movimento', 'Fluxo de caixa'),
        )


@receiver(post_save, sender=Lancamento, dispatch_uid='gerar_movimentos')
def gerar_movimentos(sender, instance, created, *args, **kwargs):
    if not created:
        return
    if instance.n_parcelas > 0: # se lançamento parcelado
        for i in range(instance.n_parcelas):
            vencimento = instance.data + timedelta(days=30 * (i + 1))
            if vencimento.weekday() == 5:
                vencimento += timedelta(days=2)
            elif vencimento.weekday() == 6:
                vencimento += timedelta(days=1)
            Movimento(
                lancamento=instance, 
                parcela=i + 1,
                valor=(instance.valor - instance.entrada) / instance.n_parcelas,
                data=vencimento).save()
        if instance.entrada > 0: # se foi dado algum valor de entrada
            atual = instance.data
            Movimento(
                lancamento=instance, 
                parcela=-1,
                valor=instance.entrada,
                data=atual,
                data_pagamento=atual,
                pago=True).save()
    else:
        vencimento = instance.data
        Movimento(
            lancamento=instance, 
            parcela=0,
            valor=instance.valor,
            data=vencimento).save()


auditlog.register(Lancamento)
auditlog.register(Movimento)
