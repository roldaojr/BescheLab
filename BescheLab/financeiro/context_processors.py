from django.utils.timezone import now
from .models import Movimento


def pagamentos_atrasados(request):
    pendentes = Movimento.objects.select_related('lancamento').filter(
        lancamento__ponto_coleta=request.ponto_coleta,
        data__lt=now(), pago=False)
    return {'pagamentos_atrasados': pendentes}
