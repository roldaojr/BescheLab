# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function
from django import forms
from django_select2.forms import ModelSelect2Widget
from .models import Lancamento, Pessoa


class LancamentoForm(forms.ModelForm):
    n_parcelas = forms.ChoiceField(
        choices=[(i, i) for i in range(1, 13)], required=False, label='Parcelas')

    entrada = forms.DecimalField(max_digits=10, decimal_places=2, required=False)

    class Meta:
        model = Lancamento
        fields = ('pessoa', 'descricao', 'valor', 'n_parcelas', 'entrada')
        widgets = {
            'pessoa': ModelSelect2Widget(model=Pessoa,
                                         search_fields=['nome__icontains']),
            'descricao': forms.widgets.Textarea(attrs={
                'rows': 3, 'class': 'resizable_textarea'})
        }

    class Media:
        js = ('js/parcelado.js',)


class DatasForm(forms.Form):
    data_inicial = forms.DateField(
        widget=forms.widgets.DateInput(attrs={'class': 'date-picker'}),
        label='Início')
    data_final = forms.DateField(
        widget=forms.widgets.DateInput(attrs={'class': 'date-picker'}),
        label='Fim')

    def clean(self):
        cleaned_data = super(DatasForm, self).clean()
        if cleaned_data['data_final'] < cleaned_data['data_inicial']:
            raise forms.ValidationError(
                'Data inicial deve ser anterior a final')
        return cleaned_data

    class Media:
        js = ('js/date-picker.js',)
