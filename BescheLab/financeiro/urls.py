# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf.urls import url
from .views import lancamento, movimento

urlpatterns = [
    url(r'^lancamento/(?P<tipo>\d+)/pendentes$', lancamento.listar,
        name='lancamento_listar'),
    url(r'^lancamento/(?P<tipo>\d+)/adicionar$', lancamento.adicionar,
        name='lancamento_adicionar'),
    url(r'^lancamento/(?P<pk>\d+)/detalhar$', lancamento.detalhar,
        name='lancamento_detalhar'),
    url(r'^lancamento/(?P<pk>\d+)/apagar$', lancamento.apagar,
        name='lancamento_apagar'),
    url(r'^lancamento/(?P<id>\d+)/pagar$', lancamento.pagar,
        name='lancamento_pagar'),
    url(r'^lancamento/relatorio$', lancamento.relatorio,
        name='lancamento_relatorio'),
    url(r'^lancamento/relatorio/imprimir$', lancamento.relatorio,
        {'modo': 'imprimir'}, name='lancamento_relatorio_imprimir'),
    url(r'^lancamento/pagar-parcela/(?P<pk>\d+)$', movimento.pagar,
        name='movimento_pagar'),
    url(r'^movimento/comprovante/(?P<pk>\d+)$', movimento.comprovante,
        name='movimento_comprovante'),
    url(r'^movimento/pendentes/$', movimento.pendentes,
        name='movimento_pendentes'),
    url(r'^movimento/relatorio/$', movimento.relatorio,
        name='movimento_relatorio'),
    url(r'^movimento/relatorio/imprimir$', movimento.relatorio,
        {'modo': 'imprimir'}, name='movimento_relatorio_imprimir'),
]
