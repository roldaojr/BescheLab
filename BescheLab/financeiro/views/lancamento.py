# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from datetime import date
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required, permission_required
from django.utils.timezone import now
from ..forms import LancamentoForm, DatasForm
from ..models import Lancamento
from ..tables import LancamentoTable, ListarLancamentoTable


@login_required
@permission_required('financeiro.add_lancamento', raise_exception=True)
def pagar(request, pk):
    lancamento = Lancamento.objects.get(pk=pk)
    for movimento in lancamento.movimentos.all():
        movimento.pago = True
        movimento.save()
    return redirect('lancamento_adicionar', tipo=lancamento.tipo)


@login_required
@permission_required('financeiro.add_lancamento', raise_exception=True)
def adicionar(request, tipo):
    if request.method == 'POST':
        form = LancamentoForm(request.POST)
        if form.is_valid():
            lancamento = form.save(commit=False)
            lancamento.tipo = tipo
            lancamento.data = date.today()
            lancamento.ponto_coleta = request.ponto_coleta
            lancamento.save()
            return redirect('lancamento_listar', tipo=tipo)
    else:
        initial = {}
        if 'form' in request.session:
            initial = request.session['form']
            del request.session['form']
        form = LancamentoForm(initial=initial)

    return render(request, 'lancamento/adicionar.html', {
        'form': form,
        'tipo_lancamento': dict(Lancamento.Tipos.choices)[int(tipo)]
    })


@login_required
@permission_required('financeiro.add_lancamento', raise_exception=True)
def detalhar(request, pk):
    lancamento = Lancamento.objects.get(pk=pk)
    return render(request, 'lancamento/detalhar.html',
                  {'lancamento': lancamento})


@login_required
@permission_required('financeiro.delete_lancamento', raise_exception=True)
def apagar(request, pk):
    if request.method == 'POST':
        lancamento = Lancamento.objects.get(pk=pk)
        lancamento.delete()
    return redirect('lancamento_listar', tipo=lancamento.tipo)


@login_required
@permission_required('financeiro.add_lancamento', raise_exception=True)
def listar(request, tipo):
    lancamentos = Lancamento.objects.filter(pago=False, tipo=tipo,
                                            ponto_coleta=request.ponto_coleta)
    table = ListarLancamentoTable(lancamentos)

    return render(request, 'lancamento/listar.html', {
        'lancamentos': lancamentos, 'table': table
    })


@login_required
@permission_required('financeiro.relatorio_lancamento', raise_exception=True)
def relatorio(request, modo=None):
    data_inicial = request.GET.get('data_inicial', '')
    data_final = request.GET.get('data_final', '')

    if data_inicial and data_final:
        form = DatasForm(request.GET)
        if form.is_valid():
            lancamentos = Lancamento.objects.filter(
                ponto_coleta=request.ponto_coleta,
                data__gte=form.cleaned_data['data_inicial'],
                data__lte=form.cleaned_data['data_final'])
        else:
            lancamentos = []
    else:
        form = DatasForm()
        lancamentos = Lancamento.objects.filter(
            ponto_coleta=request.ponto_coleta,
            data__gte=now(), data__lte=now())

    if modo == 'imprimir':
        context = {'lancamentos': lancamentos}
        template = 'lancamento/relatorio-imprimir.html'
    else:
        table = LancamentoTable(lancamentos)
        context = {'form': form, 'table': table, 'lancamentos': lancamentos}
        template = 'lancamento/relatorio.html'
    return render(request, template, context)
