# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required, permission_required
from django.http import JsonResponse
from django.utils.timezone import now
from django.db.models import Sum
from ..models import Movimento, Lancamento
from ..forms import DatasForm


@login_required
@permission_required('financeiro.add_lancamento', raise_exception=True)
def pagar(request, pk):
    movimento = Movimento.objects.get(pk=pk)
    movimento.pago = True
    movimento.data_pagamento = now()
    movimento.save()
    return redirect('lancamento_detalhar', pk=movimento.lancamento_id)


@login_required
def pendentes(request):
    paciente_id = request.GET.get('paciente_id', None)
    if paciente_id:
        pendencias = Movimento.objects.filter(
            lancamento__pessoa_id=paciente_id, pago=False).count()
    else:
        pendencias = 0
    return JsonResponse({'pendencias': pendencias})


@login_required
def comprovante(request, pk):
    movimento = Movimento.objects.get(pk=pk)
    if movimento.pago:
        return render(request, 'movimento/comprovante.html',
                      {'movimento': movimento})
    else:
        return redirect('lancamento_detalhar', pk=movimento.lancamento_id)


@login_required
@permission_required('financeiro.relatorio_movimento', raise_exception=True)
def relatorio(request, modo=None):
    data_inicial = request.GET.get('data_inicial', '')
    data_final = request.GET.get('data_final', '')
    context = {}

    if data_inicial and data_final:
        form = DatasForm(request.GET)
        if form.is_valid():
            movimentos = Movimento.objects.filter(
                lancamento__ponto_coleta=request.ponto_coleta,
                data_pagamento__gte=form.cleaned_data['data_inicial'],
                data_pagamento__lte=form.cleaned_data['data_final'], pago=True)
            receitas = movimentos.filter(
                lancamento__tipo=Lancamento.Tipos.Receita).aggregate(
                total=Sum('valor'))['total'] or 0
            despesas = movimentos.filter(
                lancamento__tipo=Lancamento.Tipos.Despesa).aggregate(
                total=Sum('valor'))['total'] or 0
            movimentos_total = receitas - despesas
            context.update({'movimentos': movimentos,
                            'movimentos_total': movimentos_total})
    else:
        form = DatasForm(initial={'data_inicial': now(),
                                  'data_final': now()})
        movimentos = Movimento.objects.filter(
            lancamento__ponto_coleta=request.ponto_coleta,
            data_pagamento__gte=now(), data_pagamento__lte=now(), pago=True)
        context.update({'movimentos': movimentos,
                        'movimentos_total': 0})

    context.update({'form': form})

    if modo == 'imprimir':
        template = 'movimento/relatorio-imprimir.html'
    else:
        template = 'movimento/relatorio.html'
    return render(request, template, context)
