from .models import PontoColeta


def ponto_de_coleta(get_response):
    def middleware(request):
        try:
            ponto_coleta_pk = request.session.get('ponto_coleta_pk')
            ponto_coleta = PontoColeta.objects.get(pk=ponto_coleta_pk)
        except:
            if request.user.has_perm('pontos_coleta.can_change_pontocoleta'):
                ponto_coleta = PontoColeta.objects.first()
            elif hasattr(request.user, 'pontos_coleta'):
                ponto_coleta = request.user.pontos_coleta.first()
            else:
                ponto_coleta = None

        request.ponto_coleta = ponto_coleta
        response = get_response(request)
        return response
    return middleware
