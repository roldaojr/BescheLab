from django import forms
from django_select2.forms import ModelSelect2Widget
from .models import PontoColeta
from ..pessoa.models import Municipio


class PontoColetaForm(forms.ModelForm):
    class Meta:
        model = PontoColeta
        fields = ('__all__')
        widgets = {
            'municipio': ModelSelect2Widget(
                model=Municipio, search_fields=['nome__icontains']),
        }
