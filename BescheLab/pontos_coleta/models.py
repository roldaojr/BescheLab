# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from auditlog.registry import auditlog


@python_2_unicode_compatible
class PontoColeta(models.Model):
    nome = models.CharField(max_length=50, unique=True)
    # Endereço
    logradouro = models.CharField(max_length=50, blank=True, null=True)
    numero = models.CharField('Número', max_length=10, blank=True, null=True)
    complemento = models.CharField(max_length=50, blank=True, null=True)
    bairro = models.CharField(max_length=50, blank=True, null=True)
    municipio = models.ForeignKey('pessoa.Municipio', null=True)

    def __str__(self):
        return self.nome

    class Meta:
        verbose_name = 'Ponto de coleta'
        default_permissions = []
        permissions = (
            ('change_pontocoleta', "Cadastrar"),
        )


auditlog.register(PontoColeta)
