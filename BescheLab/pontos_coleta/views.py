# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required, permission_required
from .models import PontoColeta
from .forms import PontoColetaForm


@login_required
@permission_required('pontos_coleta.change_pontocoleta')
def listar(request):
    pontos = PontoColeta.objects.all()
    return render(request, 'pontos_coleta/listar.html',
                  {'pontos': pontos})


@login_required
@permission_required('pontos_coleta.change_pontocoleta')
def adicionar(request):
    if request.method == 'POST':
        form = PontoColetaForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse('pontos_coleta:listar'))
    else:
        form = PontoColetaForm()
    return render(request, 'pontos_coleta/form.html',
                  {'form': form})


@login_required
@permission_required('pontos_coleta.change_pontocoleta')
def editar(request, pk):
    ponto = PontoColeta.objects.get(pk=pk)
    if request.method == 'POST':
        form = PontoColetaForm(request.POST, instance=ponto)
        if form.is_valid():
            form.save()
            return redirect(reverse('pontos_coleta:listar'))
    else:
        form = PontoColetaForm(instance=ponto)
    return render(request, 'pontos_coleta/form.html',
                  {'form': form, 'object': ponto})


@login_required
@permission_required('pontos_coleta.change_pontocoleta')
def apagar(request, pk):
    if request.method == 'POST':
        ponto = PontoColeta.objects.get(pk=pk)
        ponto.delete()
    return redirect(reverse('pontos_coleta:listar'))


@login_required
def selecionar(request, pk):
    request.session['ponto_coleta_pk'] = pk
    return redirect(reverse('index'))
