from .models import PontoColeta


def pontos_do_coleta(request):
    if request.user.has_perm('pontos_coleta.can_change_pontocoleta'):
        pontos_coleta = PontoColeta.objects.all()
    elif hasattr(request.user, 'pontos_coleta'):
        pontos_coleta = request.user.pontos_coleta.all()
    else:
        pontos_coleta = []
    return {'pontos_coleta': pontos_coleta}
