from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^adicionar$', adicionar, name='adicionar'),
    url(r'^editar/(?P<pk>\d+)$', editar, name='editar'),
    url(r'^apagar/(?P<pk>\d+)$', apagar, name='apagar'),
    url(r'^listar$', listar, name='listar'),
    url(r'^selecionar/(?P<pk>\d+)$', selecionar, name='selecionar')
]
