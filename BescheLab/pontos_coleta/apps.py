# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.apps import AppConfig


class PontoColetaConfig(AppConfig):
    name = 'BescheLab.pontos_coleta'
    verbose_name = 'Pontos de Coleta'
