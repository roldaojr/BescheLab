# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-01 20:45
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pontos_coleta', '0002_auto_20170301_1512'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='pontocoleta',
            options={'default_permissions': [], 'permissions': (('change', 'Cadastrar'),), 'verbose_name': 'Ponto de coleta'},
        ),
    ]
