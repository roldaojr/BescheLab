# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-11 22:05
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pontos_coleta', '0004_auto_20170306_0920'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='pontocoleta',
            options={'default_permissions': [], 'permissions': (('change_pontocoleta', 'Cadastrar'),), 'verbose_name': 'Ponto de coleta'},
        ),
    ]
