from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth.views import login, logout_then_login

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^select2/', include('django_select2.urls')),
    url(r'^login', login, name='login'),
    url(r'^logout', logout_then_login, name='logout'),
    url(r'^table/', include('table.urls')),
    url(r'^pontos_coleta/', include('BescheLab.pontos_coleta.urls',
                                    namespace='pontos_coleta')),
    url(r'', include('BescheLab.layout.urls')),
    url(r'^pessoa/', include('BescheLab.pessoa.urls')),
    url(r'^financeiro/', include('BescheLab.financeiro.urls')),
    url(r'^laboratorio/', include('BescheLab.laboratorio.urls')),
]
