# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.models import Group
from ..forms import GrupoForm


@login_required()
@permission_required('auth.change_group', raise_exception=True)
def listar(request):
    grupos = Group.objects.all()
    return render(request, 'pessoa/grupo/listar.html', {'grupos': grupos})


@login_required()
@permission_required('auth.change_group', raise_exception=True)
def editar(request, id):
    grupo = Group.objects.get(pk=id)
    if request.method == 'POST':
        form = GrupoForm(request.POST, instance=grupo)
        if form.is_valid():
            form.save()
            return redirect(reverse('grupo_listar'))
    else:
        form = GrupoForm(instance=grupo)

    return render(request, 'pessoa/grupo/form.html',
                  {'object': grupo, 'form': form})
