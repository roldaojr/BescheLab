# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required, permission_required
from ..models import Fornecedor
from ..forms import FornecedorForm
from ..tables import FornecedorTable


@login_required
@permission_required('pessoa.change_fornecedor', raise_exception=True)
def adicionar(request):
    if request.method == 'POST':
        form = FornecedorForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse('fornecedor_listar'))
    else:
        form = FornecedorForm()
    return render(request, 'pessoa/fornecedor/form.html', {'form': form})


@login_required
@permission_required('pessoa.change_fornecedor', raise_exception=True)
def editar(request, id):
    fornecedor = Fornecedor.objects.get(id=id)

    if request.method == 'POST':
        form = FornecedorForm(request.POST, instance=fornecedor)
        if form.is_valid():
            form.save()
            return redirect(reverse('fornecedor_listar'))
    else:
        form = FornecedorForm(instance=fornecedor)
    return render(request, 'pessoa/fornecedor/form.html',
                  {'form': form, 'object': fornecedor})


@login_required
@permission_required('pessoa.delete_fornecedor', raise_exception=True)
def apagar(request, id):
    if request.method == 'POST':
        fornecedor = Fornecedor.objects.get(id=id)
        fornecedor.delete()
    return redirect(reverse('fornecedor_listar'))


@login_required
@permission_required('pessoa.change_fornecedor', raise_exception=True)
def listar(request):
    fornecedores = Fornecedor.objects.all()
    table = FornecedorTable()
    return render(request, 'pessoa/fornecedor/listar.html',
                  {'table': table, 'fornecedors': fornecedores})
