# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required, permission_required
from ..models import Paciente
from ..forms import PessoaForm
from ..tables import PacienteTable


@login_required
@permission_required('pessoa.change_paciente', raise_exception=True)
def adicionar(request):
    if request.method == 'POST':
        form = PessoaForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse('pessoa_listar'))
    else:
        form = PessoaForm()
    return render(request, 'pessoa/paciente/form.html', {'form': form})


@login_required
@permission_required('pessoa.change_paciente', raise_exception=True)
def editar(request, id):
    pessoa = Paciente.objects.get(id=id)

    if request.method == 'POST':
        form = PessoaForm(request.POST, instance=pessoa)
        if form.is_valid():
            form.save()
            return redirect(reverse('pessoa_listar'))
    else:
        form = PessoaForm(instance=pessoa)
    return render(request, 'pessoa/paciente/form.html',
                  {'form': form, 'object': pessoa})


@login_required
@permission_required('pessoa.delete_paciente', raise_exception=True)
def apagar(request, id):
    if request.method == 'POST':
        pessoa = Paciente.objects.get(id=id)
        pessoa.delete()
    return redirect(reverse('pessoa_listar'))


@login_required
@permission_required('pessoa.change_paciente', raise_exception=True)
def listar(request):
    pessoas = Paciente.objects.all()
    table = PacienteTable()
    return render(request, 'pessoa/paciente/listar.html',
                  {'table': table, 'pessoas': pessoas})
