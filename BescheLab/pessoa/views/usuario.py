# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.contrib.auth.forms import AdminPasswordChangeForm
from django.contrib.auth.decorators import login_required, permission_required
from ..models import Usuario
from ..forms import UsuarioForm
from ..tables import UsuarioTable


@login_required
@permission_required('pessoa.change_usuario', raise_exception=True)
def listar(request):
    table = UsuarioTable()
    usuarios = Usuario.objects.all()
    return render(request, 'pessoa/usuario/listar.html',
                  {'table': table, 'usuarios': usuarios})


@login_required
@permission_required('pessoa.change_usuario', raise_exception=True)
def adicionar(request):
    if request.method == 'POST':
        form = UsuarioForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse('usuario_listar'))
    else:
        form = UsuarioForm()

    return render(request, 'pessoa/usuario/form.html', {'form': form})


@login_required
@permission_required('pessoa.change_usuario', raise_exception=True)
def editar(request, id):
    usuario = Usuario.objects.get(pk=id)
    if request.method == 'POST':
        form = UsuarioForm(request.POST, instance=usuario)
        if form.is_valid():
            form.save()
            return redirect(reverse('usuario_listar'))
    else:
        form = UsuarioForm(instance=usuario)

    return render(request, 'pessoa/usuario/form.html',
                  {'object': usuario, 'form': form})


@login_required
@permission_required('pessoa.change_usuario', raise_exception=True)
def definir_senha(request, id):
    usuario = Usuario.objects.get(pk=id)
    if request.method == 'POST':
        form = AdminPasswordChangeForm(usuario, request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse('usuario_listar'))
    else:
        form = AdminPasswordChangeForm(usuario)

    return render(request, 'pessoa/usuario/form.html',
                  {'object': usuario, 'form': form})


@login_required
@permission_required('pessoa.change_usuario', raise_exception=True)
def apagar(request, pk):
    if request.method == 'POST':
        exame = Usuario.objects.get(pk=pk)
        exame.delete()
    return redirect(reverse('usuario_listar'))
