from django.conf.urls import url
from .views import pessoa, fornecedor, usuario, grupo


urlpatterns = [
    url(r'^usuario/$', usuario.listar,
        name='usuario_listar'),
    url(r'^usuario/adicionar$', usuario.adicionar,
        name='usuario_adicionar'),
    url(r'^usuario/(?P<id>\d+)/editar$', usuario.editar,
        name='usuario_editar'),
    url(r'^usuario/(?P<pk>\d+)/apagar$', usuario.apagar,
        name='usuario_apagar'),
    url(r'^usuario/(?P<id>\d+)/definir_senha$', usuario.definir_senha,
        name='usuario_definir_senha'),

    url(r'^paciente/adicionar$', pessoa.adicionar,
        name='pessoa_adicionar'),
    url(r'^paciente/(?P<id>\d+)/editar$', pessoa.editar,
        name='pessoa_editar'),
    url(r'^paciente/(?P<id>\d+)/apagar$', pessoa.apagar,
        name='pessoa_apagar'),
    url(r'^pacientes$', pessoa.listar,
        name='pessoa_listar'),

    url(r'^fornecedor/adicionar$', fornecedor.adicionar,
        name='fornecedor_adicionar'),
    url(r'^fornecedor/(?P<id>\d+)/editar$', fornecedor.editar,
        name='fornecedor_editar'),
    url(r'^fornecedor/(?P<id>\d+)/apagar$', fornecedor.apagar,
        name='fornecedor_apagar'),
    url(r'^fornecedor/listar$', fornecedor.listar,
        name='fornecedor_listar'),

    url(r'^grupos/(?P<id>\d+)/editar$', grupo.editar,
        name='grupo_editar'),
    url(r'^grupos$', grupo.listar,
        name='grupo_listar'),
]
