# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-11 15:01
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pessoa', '0008_auto_20170311_1109'),
    ]

    operations = [
        migrations.AlterField(
            model_name='usuario',
            name='pontos_coleta',
            field=models.ManyToManyField(related_name='usuarios', to='pontos_coleta.PontoColeta', verbose_name='ponto de coleta'),
        ),
    ]
