# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import migrations
from django.contrib.auth.models import Permission


def forward(apps, schema_editor):
    Permission.objects.all().delete()


class Migration(migrations.Migration):

    dependencies = [
        ('pessoa', '0006_auto_20170301_1745'),
    ]

    operations = [
        migrations.RunPython(forward)
    ]
