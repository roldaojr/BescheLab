# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import migrations


def forward(apps, schema_editor):
    Permission = apps.get_model('auth', 'Permission')
    Permission.objects.filter(
        codename__in=['add', 'change', 'delete']).delete()


def backward(apps, schema_editor):
    pass


class Migration(migrations.Migration):

    dependencies = [
        ('pessoa', '0013_auto_20170312_1026'),
    ]

    operations = [
        migrations.RunPython(forward, backward)
    ]
