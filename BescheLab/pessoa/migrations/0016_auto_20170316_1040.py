# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-16 13:40
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pessoa', '0015_auto_20170316_1025'),
    ]

    operations = [
        migrations.AlterField(
            model_name='usuario',
            name='tipo',
            field=models.IntegerField(choices=[(1, 'Administrador'), (2, 'Gerente'), (3, 'Atendente'), (4, 'Analista')], default=3),
        ),
    ]
