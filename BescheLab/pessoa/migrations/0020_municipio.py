# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import migrations


def forward(apps, schema_editor):
    Pessoa = apps.get_model('pessoa', 'Pessoa')
    Municipio = apps.get_model('pessoa', 'Municipio')
    for pessoa in Pessoa.objects.all():
        try:
            municipio = Municipio.objects.select_related('uf').get(
                nome=pessoa.cidade, uf__nome=pessoa.uf)
        except Municipio.DoesNotExist:
            continue
        pessoa.municipio = municipio
        pessoa.save()


def backward(apps, schema_editor):
    Pessoa = apps.get_model('pessoa', 'Pessoa')
    for pessoa in Pessoa.objects.select_related('municipio',
                                                'municipio__uf').all():
        if pessoa.municipio and not pessoa.cidade:
            pessoa.cidade = pessoa.municipio.nome
            pessoa.uf = pessoa.municipio.uf.sigla
            pessoa.save()


class Migration(migrations.Migration):

    dependencies = [
        ('pessoa', '0019_auto_20170325_1253'),
    ]

    operations = [
        migrations.RunPython(forward, backward)
    ]
