# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-17 13:37
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pessoa', '0016_auto_20170316_1040'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pessoa',
            name='bairro',
            field=models.CharField(max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='pessoa',
            name='cidade',
            field=models.CharField(max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='pessoa',
            name='cpf_cnpj',
            field=models.CharField(blank=True, max_length=18, null=True, verbose_name='CPF/CPNJ'),
        ),
        migrations.AlterField(
            model_name='pessoa',
            name='logradouro',
            field=models.CharField(max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='pessoa',
            name='numero',
            field=models.CharField(max_length=10, null=True, verbose_name='N\xfamero'),
        ),
        migrations.AlterField(
            model_name='pessoa',
            name='telefone',
            field=models.CharField(max_length=12, null=True),
        ),
        migrations.AlterField(
            model_name='pessoa',
            name='uf',
            field=models.CharField(max_length=2, null=True, verbose_name='UF'),
        ),
    ]
