# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-01 14:38
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pontos_coleta', '0001_initial'),
        ('pessoa', '0004_auto_20170227_1343'),
    ]

    operations = [
        migrations.AddField(
            model_name='usuario',
            name='pontos_coleta',
            field=models.ManyToManyField(related_name='usuarios', to='pontos_coleta.PontoColeta'),
        ),
    ]
