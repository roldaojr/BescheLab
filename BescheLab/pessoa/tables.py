# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from table import Table
from table.columns import Column
from table.columns.linkcolumn import LinkColumn, Link
from table.utils import A
from ..layout.tables import BooleanColumn, DateColumn
from .models import Usuario, Paciente, Fornecedor


class PacienteTable(Table):
    nome = LinkColumn(field='nome', header='Nome', links=[
        Link(viewname='pessoa_editar', args=(A('pk'),), text=A('nome'))])
    telefone = Column(field='telefone', header='Telefone')
    email = Column(field='email', header='E-mail')

    class Meta:
        model = Paciente
        ajax = True
        attrs = {
            'class': 'table-striped dt-responsive nowrap rowlink',
            'data-link': 'row'
        }


class FornecedorTable(Table):
    nome = LinkColumn(field='nome', header='Nome', links=[
        Link(viewname='fornecedor_editar', args=(A('pk'),), text=A('nome'))])
    telefone = Column(field='telefone', header='Telefone')
    email = Column(field='email', header='E-mail')

    class Meta:
        model = Fornecedor
        ajax = True
        attrs = {
            'class': 'table-striped dt-responsive nowrap rowlink',
            'data-link': 'row'
        }


class UsuarioTable(Table):
    login = LinkColumn(field='login', header='Login', links=[
        Link(viewname='usuario_editar', args=(A('pk'),), text=A('login'))])
    tipo = Column(field='tipo', header='Tipo')
    ativo = BooleanColumn(field='ativo', header='Ativo')
    admissao = DateColumn(field='admissao', header='Admissão')

    class Meta:
        model = Usuario
        ajax = True
        attrs = {
            'class': 'table-striped dt-responsive nowrap rowlink',
            'data-link': 'row'
        }
