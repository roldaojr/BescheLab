# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.core.management import BaseCommand
from urllib2 import urlopen
import json
from ...models import Municipio, UF

JSON_URL = 'https://gist.githubusercontent.com/letanure/3012978/raw/36fc21d9e2fc45c078e0e0e07cce3c81965db8f9/estados-cidades.json'


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('filename', nargs='?')

    def handle(self, *args, **kwargs):
        filename = kwargs.get('filename')
        if filename:
            fp = open(filename)
        else:
            self.stdout.write('Fazendo download %s' % JSON_URL)
            fp = urlopen(JSON_URL)
        self.stdout.write(
            'Importando cidades e estados do arquivo %s' % filename)
        json_str = fp.read().decode('utf-8')
        data = json.loads(json_str)
        for uf in data['estados']:
            uf_obj, created = UF.objects.update_or_create(
                nome=uf['nome'], defaults={'sigla': uf['sigla']})
            self.stdout.write('  %s, %s...' % (
                uf['nome'], uf['sigla']))
            for municipio in uf['cidades']:
                Municipio.objects.update_or_create(nome=municipio, uf=uf_obj)
        self.stdout.write(self.style.SUCCESS('Concluido.'))
