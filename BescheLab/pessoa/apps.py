# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.apps import AppConfig


def associar_grupo(sender, request, user, **kwargs):
    from django.contrib.auth.models import Group
    grupo_nome = user.get_tipo_display()
    grupo, c = Group.objects.get_or_create(name=grupo_nome)
    if grupo not in user.groups.all():
        user.groups.clear()
        user.groups.add(grupo)


def criar_grupos(*args, **kwargs):
    from django.contrib.auth.models import Group
    from .models import Usuario
    for tipo, nome in Usuario.Tipos.choices:
        grupo, c = Group.objects.get_or_create(name=nome)


class PessoaConfig(AppConfig):
    name = 'BescheLab.pessoa'
    verbose_name = 'Pessoas'

    def ready(self):
        from django.db.models.signals import post_migrate
        from django.contrib.auth.signals import user_logged_in
        user_logged_in.connect(associar_grupo)
        post_migrate.connect(criar_grupos)
