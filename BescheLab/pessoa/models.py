# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import (AbstractBaseUser, BaseUserManager,
                                        PermissionsMixin)
from django.utils.encoding import python_2_unicode_compatible
from djchoices import DjangoChoices, ChoiceItem
from django.utils.timezone import now
from auditlog.registry import auditlog
from ..pontos_coleta.models import PontoColeta


class UsuarioManager(BaseUserManager):
    def create_user(self, login, password=None):
        """
        Cria e salva usuário a partir de login e senha
        """
        if not login:
            raise ValueError('Usuário deve possuir login')

        user = self.model(login=login)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, login, password=None):
        """
        Cria e salva super usuário a partir de login e senha
        """
        user = self.create_user(login, password=password)
        user.tipo = Usuario.Tipos.administrador
        user.save(using=self._db)
        return user


class PacienteManager(models.Manager):
    def get_queryset(self):
        return super(PacienteManager, self).get_queryset().filter(tipo='p')


class FornecedorManager(models.Manager):
    def get_queryset(self):
        return super(FornecedorManager, self).get_queryset().filter(tipo='f')


@python_2_unicode_compatible
class UF(models.Model):
    nome = models.CharField(max_length=100, unique=True)
    sigla = models.CharField(max_length=2, unique=True)

    def __str__(self):
        return '%s, %s' % (self.nome, self.sigla)


@python_2_unicode_compatible
class Municipio(models.Model):
    nome = models.CharField(max_length=100)
    uf = models.ForeignKey(UF, related_name='municipios')

    def __str__(self):
        return '%s, %s' % (self.nome, self.uf.sigla)


@python_2_unicode_compatible
class Pessoa(models.Model):
    class Sexos(DjangoChoices):
        masculino = ChoiceItem(1, 'Masculino')
        feminino = ChoiceItem(2, 'Feminino')

    nome = models.CharField(max_length=50)
    tipo = models.CharField(max_length=1, default='p', editable=False)
    telefone = models.CharField(max_length=12, null=True)
    email = models.EmailField('e-mail', max_length=50, blank=True, null=True)
    # Pessoa fisica
    cpf_cnpj = models.CharField('CPF/CPNJ', max_length=18, blank=True,
                                null=True)
    cns = models.CharField('CNS', max_length=15, blank=True, null=True)
    data_nascimento = models.DateField('Nascimento', blank=True, null=True)
    sexo = models.IntegerField('sexo', choices=Sexos.choices,
                               blank=True, null=True)
    # Endereço
    logradouro = models.CharField(max_length=50, null=True)
    numero = models.CharField('Número', max_length=10, null=True)
    complemento = models.CharField(max_length=50, blank=True, null=True)
    bairro = models.CharField(max_length=50, null=True)
    municipio = models.ForeignKey(Municipio, null=True)

    def __str__(self):
        return self.nome

    class Meta:
        verbose_name = 'Pessoa'
        default_permissions = []


class Paciente(Pessoa):
    objects = PacienteManager()

    def save(self, *kargs, **kwargs):
        self.tipo = 'p'
        return super(Paciente, self).save(*kargs, **kwargs)

    class Meta:
        proxy = True
        default_permissions = []
        permissions = (
            ('change_paciente', "Cadastrar paciente"),
            ('delete_paciente', "Excluir paciente"),
        )


class Fornecedor(Pessoa):
    objects = FornecedorManager()

    def save(self, *kargs, **kwargs):
        self.tipo = 'f'
        return super(Fornecedor, self).save(*kargs, **kwargs)

    class Meta:
        proxy = True
        default_permissions = []
        permissions = (
            ('change_fornecedor', "Cadastrar fornecedor"),
            ('delete_fornecedor', "Excluir fornecedor"),
        )


@python_2_unicode_compatible
class Usuario(AbstractBaseUser, PermissionsMixin):
    class Tipos(DjangoChoices):
        administrador = ChoiceItem(1, 'Administrador')
        gerente = ChoiceItem(2, 'Gerente')
        atentente = ChoiceItem(3, 'Atendente')
        analista = ChoiceItem(4, 'Analista')

    pessoa = models.OneToOneField(Pessoa, null=True, on_delete=models.CASCADE)
    login = models.CharField(max_length=20, unique=True)
    cbo = models.IntegerField('CBO', blank=True, null=True)
    n_conselho = models.IntegerField('Conselho de Classe',
                                     blank=True, null=True)
    admissao = models.DateField(default=now)
    ativo = models.BooleanField(default=True)
    tipo = models.IntegerField(choices=Tipos.choices, default=Tipos.atentente)
    pontos_coleta = models.ManyToManyField(
        PontoColeta, related_name='usuarios', verbose_name='ponto de coleta')

    objects = UsuarioManager()
    USERNAME_FIELD = 'login'

    def __str__(self):
        return self.get_short_name()

    @property
    def is_active(self):
        return self.ativo

    class Meta:
        verbose_name = 'Usuário'
        default_permissions = []
        permissions = (
            ('change_usuario', "Cadastrar"),
            ('log_acoes', "Relatorio de ações"),
        )

    # Para funcionamento correto do admin
    # <admin>
    @property
    def is_staff(self):
        return self.is_superuser

    @property
    def is_superuser(self):
        return self.tipo == Usuario.Tipos.administrador

    def get_short_name(self):
        if self.pessoa:
            return self.pessoa.nome.split(' ')[0]
        else:
            return self.login

    def get_full_name(self):
        if self.pessoa:
            return self.pessoa.nome
        else:
            return self.login
    # </admin>


auditlog.register(Paciente)
auditlog.register(Fornecedor)
auditlog.register(Usuario)
