# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from ...laboratorio.views.exame_marcado import listar as agenda_exames


@login_required
def index(request):
    return agenda_exames(request)


@login_required
def configuracoes(request):
    return render(request, 'configuracoes.html')
