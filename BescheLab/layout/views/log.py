# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.contrib.auth.decorators import login_required, permission_required
from auditlog.models import LogEntry


@login_required
@permission_required('pessoa.log_acoes', raise_exception=True)
def relatorio_acoes(request):
    log = LogEntry.objects.all()
    return render(request, 'relatorio-acoes.html', {'log': log})
