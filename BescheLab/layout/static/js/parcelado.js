function parcelado_toggle() {
	form = $("#lancamentoForm")
	checked = form.find("input[name=parcelado]").is(':checked')
	n_parcelas = form.find("#id_n_parcelas, #id_pagamento-n_parcelas").attr("disabled", !checked)
	entrada = form.find("#id_entrada, #id_pagamento-entrada").attr("disabled", !checked)
	if(checked) {
		n_parcelas.val(1)
		entrada.val(0)
	}else {
		n_parcelas.val(0)
		entrada.val()
	}
}

$(document).ready(function() {
	$("#lancamentoForm input[name=parcelado]").change(parcelado_toggle)
	parcelado_toggle()
})
