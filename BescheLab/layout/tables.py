# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils import formats
from django.utils.html import linebreaks
from django.utils.safestring import mark_safe
from table.columns import Column
from table.utils import Accessor


class NumberColumn(Column):
    def render(self, obj):
        value = Accessor(self.field).resolve(obj)
        return formats.number_format(value)


class DateColumn(Column):
    def render(self, obj):
        value = Accessor(self.field).resolve(obj)
        if value:
            return formats.date_format(value)
        else:
            return ''


class BooleanColumn(Column):
    def render(self, obj):
        value = Accessor(self.field).resolve(obj)
        return 'Sim' if value else 'Não'


class ManyColumn(Column):
    def __init__(self, *args, **kwargs):
        kwargs.update({'sortable': False, 'searchable': False})
        return super(ManyColumn, self).__init__(*args, **kwargs)

    def render(self, obj):
        value = Accessor(self.field).resolve(obj)
        exames = [exame.tipo for exame in value.all()]
        return ', '.join(exames)


class ManyLineColumn(ManyColumn):
    def render(self, obj):
        value = Accessor(self.field).resolve(obj)
        exames = [exame.tipo for exame in value.all()]
        return mark_safe('<br>'.join(exames))


class TextColumn(Column):
    def render(self, obj):
        value = Accessor(self.field).resolve(obj)
        return mark_safe(linebreaks(value))
