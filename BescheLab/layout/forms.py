# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils import six


class ReadOnlyFieldsMixin(object):
    """Usage:

    class MyFormAllFieldsReadOnly(ReadOnlyFieldsMixin, forms.Form):
        ...

    class MyFormSelectedFieldsReadOnly(ReadOnlyFieldsMixin, forms.Form):
        readonly_fields = ('field1', 'field2')
        ...
    """
    readonly_fields = []

    def __init__(self, *args, **kwargs):
        readonly_fields = kwargs.pop('readonly_fields', None)
        if readonly_fields:
            self.readonly_fields = list(self.readonly_fields) + readonly_fields
        super(ReadOnlyFieldsMixin, self).__init__(*args, **kwargs)
        self.define_readonly_fields()

    def define_readonly_fields(self):
        fields = [field for field_name, field in six.iteritems(self.fields)
                  if field_name in self.readonly_fields]

        map(lambda field: self._set_readonly(field), fields)

    def _set_readonly(self, field):
        field.disabled = True
