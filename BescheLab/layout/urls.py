from django.conf.urls import url
from .views import index, configuracoes, log


urlpatterns = [
    url(r'^log-acoes$', log.relatorio_acoes, name='relatorio_acoes'),
    url(r'^configuracoes$', configuracoes, name='configuracoes'),
    url(r'^$', index, name='index'),
]
