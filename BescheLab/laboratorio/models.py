# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible
from django.db import models
from auditlog.registry import auditlog
from ..pessoa.models import Pessoa
from ..pontos_coleta.models import PontoColeta


@python_2_unicode_compatible
class Exame(models.Model):
    tipo = models.CharField(max_length=250)
    codigo = models.CharField(max_length=10, null=True)
    valor = models.DecimalField(max_digits=10, decimal_places=2, default=0)

    def __str__(self):
        return '%s (%s)' % (self.tipo, self.codigo)

    class Meta:
        ordering = ('tipo',)
        verbose_name = 'Tipo de exame'
        default_permissions = []
        permissions = (
            ('change_exame', 'Cadastrar'),
            ('delete_exame', 'Excluir'),
        )


@python_2_unicode_compatible
class ExameMarcado(models.Model):
    ponto_coleta = models.ForeignKey(PontoColeta, editable=False)
    exames = models.ManyToManyField(Exame, related_name='marcados')
    paciente = models.ForeignKey(Pessoa)
    data = models.DateField()
    feito = models.BooleanField('Coletado', default=False)

    def __str__(self):
        return 'Exames %s %s' % (self.paciente, self.data)

    class Meta:
        verbose_name = 'Marcação de exame'
        default_permissions = []
        permissions = (
            ('change_examemarcado', "Marcar e Remarcar"),
            ('realizar_examemarcado', "Realizar"),
            ('delete_examemarcado', "Excluir"),
        )


auditlog.register(Exame, include_fields=('valor',))
auditlog.register(ExameMarcado)
