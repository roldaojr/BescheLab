from django.utils.timezone import now
from .models import ExameMarcado


def exames_pendentes(request):
    pendentes = ExameMarcado.objects.select_related('paciente').filter(
        ponto_coleta=request.ponto_coleta, data__lt=now(), feito=False)
    return {'exames_pendentes': pendentes}
