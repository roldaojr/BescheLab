from django import forms
from django_select2.forms import ModelSelect2Widget, ModelSelect2MultipleWidget
from django.forms.models import modelform_factory
from ..layout.forms import ReadOnlyFieldsMixin
from .models import ExameMarcado, Exame
from ..pessoa.models import Paciente

ExameForm = modelform_factory(Exame, fields=('tipo', 'valor'))


class ExameMarcadoForm(forms.ModelForm):
    class Meta:
        model = ExameMarcado
        fields = ('paciente', 'exames', 'data')
        widgets = {
            'data': forms.widgets.DateInput(attrs={'class': 'date-picker'}),
            'paciente': ModelSelect2Widget(queryset=Paciente.objects.all(),
                                           search_fields=['nome__icontains']),
            'exames': ModelSelect2MultipleWidget(
                model=Exame, search_fields=['tipo__icontains'])
        }

    class Media:
        js = ('js/date-picker.js',)


class EditarExameMarcadoForm(ReadOnlyFieldsMixin, ExameMarcadoForm):
    readonly_fields = ('paciente', 'exames')

    class Meta(ExameMarcadoForm.Meta):
        fields = ('paciente', 'exames', 'data', 'feito')
