# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.core.management import BaseCommand
import os
import re
import csv
from six.moves.urllib.parse import urlparse
from zipfile import ZipFile
from io import StringIO
from tempfile import TemporaryFile
from fixedwidth.fixedwidth import FixedWidth
import ftplib
import ftputil
from auditlog.registry import auditlog
from ...models import Exame


DATASUS_FTP_URL = 'ftp://ftp2.datasus.gov.br/pub/sistemas/tup/downloads'
DATASUS_FILE_REGEX = 'TabelaUnificada_(\d+)_v(\d+).zip'
CHUNK_SIZE = 256 * 10240


class FTPSession(ftplib.FTP):
    def __init__(self, host, userid, password, port):
        ftplib.FTP.__init__(self)
        self.connect(host, port)
        self.login(userid, password)


class Command(BaseCommand):
    def ler_csv(self, csvfile):
        dialect = csv.Sniffer().sniff(csvfile.readline())
        csvfile.seek(0)
        return csv.DictReader(csvfile, dialect=dialect)

    def ler_layout(self, layout_file):
        csvreader = self.ler_csv(layout_file)
        fw_config = {}
        for row in csvreader:
            fw_config[row['Coluna']] = {
                'start_pos': int(row['Inicio']),
                'end_pos': int(row['Fim']),
                "type": "string",
                'alignment': 'left',
                'padding': ' ',
                'required': True,
            }
        return fw_config

    def ler_procedimentos(self, fw_config, fw_file):
        self.stdout.write('Atualizando exames...')
        fw_obj = FixedWidth(fw_config)
        auditlog.unregister(Exame)
        for line in fw_file:
            fw_obj.line = line
            if fw_obj.data['CO_PROCEDIMENTO'].startswith('0202'):
                codigo = fw_obj.data['CO_PROCEDIMENTO']
                nome = fw_obj.data['NO_PROCEDIMENTO'].decode('iso8859-1')\
                                                     .encode('utf-8')
                Exame.objects.update_or_create(codigo=codigo,
                                               defaults={'tipo': nome})

    def ler_zip(self, filename):
        self.stdout.write('Lendo arquivo...')
        with ZipFile(filename, 'r') as archive:
            table_layout = StringIO(unicode(
                archive.read('tb_procedimento_layout.txt')))
            fw_config = self.ler_layout(table_layout)
            with archive.open('tb_procedimento.txt') as table_data:
                self.ler_procedimentos(fw_config, table_data)

    def download_zip(self, urlarg, name_regex=DATASUS_FILE_REGEX):
        url = urlparse(urlarg)
        if url.scheme != 'ftp':
            raise Exception('Atualmente somente URLs FTP são suportadas')
        names = []
        tmpfile = TemporaryFile(mode='w+b')
        with ftputil.FTPHost(url.hostname, 'anonymous', '', port=url.port,
                             session_factory=FTPSession) as ftp:
            if not url.path.endswith('.zip'):
                self.stdout.write('Procurando arquivo mais recente...')
                ftp.chdir(url.path)
                for name in ftp.listdir(url.path):
                    if re.match(name_regex, name):
                        names.append((name, ftp.lstat(name).st_mtime))
                filename, mtime = max(names, key=lambda a: a[1])
                self.stdout.write('Baixando arquivo %s' % filename)
            else:
                ftp.chdir(os.path.dirname(url.path))
                filename = os.path.basename(url.path)
            # gravar em arquivo temporario
            with ftp.open(filename, mode='rb') as dl:
                while True:
                    chunk = dl.read(CHUNK_SIZE)
                    if not chunk:
                        break
                    tmpfile.write(chunk)

        tmpfile.seek(0)
        return tmpfile

    def url_remota(self, url):
        url_obj = urlparse(url)
        if url_obj.scheme in ['http', 'ftp']:
            return True
        else:
            return False

    def add_arguments(self, parser):
        parser.add_argument('path', nargs='?')

    def handle(self, *args, **kwargs):
        url = kwargs.get('path') or DATASUS_FTP_URL
        if self.url_remota(url):
            self.stdout.write('Lendo exames de %s' % url)
            try:
                zf = self.download_zip(url)
            except Exception as err:
                self.stdout.write('Erro ao fazer download: %s' % err)
                return
        elif os.path.isfile(url):
            self.stdout.write('Lendo exames de %s' % url)
            zf = url
        else:
            self.stderr.write('Caminho de arquivo inválido: %s' % url)
            return
        return self.ler_zip(zf)
