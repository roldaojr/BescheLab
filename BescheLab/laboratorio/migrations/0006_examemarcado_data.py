# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import migrations


def forward(apps, schema_editor):
    ExameMarcado = apps.get_model("laboratorio", "ExameMarcado")
    for em in ExameMarcado.objects.all():
        em.exames.add(em.exame)


class Migration(migrations.Migration):

    dependencies = [
        ('laboratorio', '0005_examemarcado_exames'),
    ]

    operations = [
        migrations.RunPython(forward)
    ]
