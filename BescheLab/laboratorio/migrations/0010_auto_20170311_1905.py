# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-11 22:05
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('laboratorio', '0009_auto_20170304_1226'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='exame',
            options={'default_permissions': [], 'permissions': (('change_exame', 'Cadastrar'), ('delete_exame', 'Excluir')), 'verbose_name': 'Tipo de exame'},
        ),
        migrations.AlterModelOptions(
            name='examemarcado',
            options={'default_permissions': [], 'permissions': (('change_examemarcado', 'Realizar'), ('delete_examemarcado', 'Excluir')), 'verbose_name': 'Marca\xe7\xe3o de exame'},
        ),
    ]
