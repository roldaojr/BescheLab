from django.conf.urls import url
from .views import exame, exame_marcado


urlpatterns = [
    url(r'^exame/adicionar$', exame.adicionar,
        name='exame_adicionar'),
    url(r'^exame/editar/(?P<pk>\d+)$', exame.editar,
        name='exame_editar'),
    url(r'^exame/apagar/(?P<pk>\d+)$', exame.apagar,
        name='exame_apagar'),
    url(r'^exame/detalhar/(?P<pk>\d+)$', exame.detalhar,
        name='exame_detalhar'),
    url(r'^exame/listar$', exame.listar,
        name='exame_listar'),

    url(r'^exame_marcado/adicionar$', exame_marcado.adicionar,
        name='exame_marcado_adicionar'),
    url(r'^exame_marcado/editar/(?P<pk>\d+)$', exame_marcado.editar,
        name='exame_marcado_editar'),
    url(r'^exame_marcado/apagar/(?P<pk>\d+)$', exame_marcado.apagar,
        name='exame_marcado_apagar'),
    url(r'^exame_marcado/listar$', exame_marcado.listar,
        name='exame_marcado_listar'),
]
