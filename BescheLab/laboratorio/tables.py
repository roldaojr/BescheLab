# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from table import Table
from table.columns import Column
from table.columns.linkcolumn import LinkColumn, Link
from table.utils import A
from .models import ExameMarcado, Exame
from ..layout.tables import NumberColumn, DateColumn, ManyLineColumn


class ExameTable(Table):
    tipo = LinkColumn(field='tipo', header='Tipo', links=[
        Link(viewname='exame_editar', args=(A('pk'),), text=A('tipo'))])
    codigo = Column(field='codigo', header='Código')
    valor = NumberColumn(field='valor', header='Valor')

    class Meta:
        model = Exame
        ajax = True
        attrs = {
            'class': 'table-striped dt-responsive nowrap rowlink',
            'data-link': 'row'
        }


class ExameMarcadoTable(Table):
    paciente = LinkColumn(field='paciente.nome', header='Paciente', links=[
        Link(viewname='exame_marcado_editar', args=(A('pk'),),
             text=A('paciente.nome'))])
    data = DateColumn(field='data', header='Data')
    exames = ManyLineColumn(field='exames', header='Exames')

    class Meta:
        model = ExameMarcado
        attrs = {
            'class': 'table-striped dt-responsive nowrap rowlink',
            'data-link': 'row'
        }
