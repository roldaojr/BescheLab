# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required, permission_required
from ..models import Exame
from ..forms import ExameForm
from ..tables import ExameTable


@login_required
@permission_required('laboratorio.change_exame', raise_exception=True)
def listar(request):
    table = ExameTable()
    exames = Exame.objects.all()
    return render(request, 'laboratorio/exame/listar.html',
                  {'table': table, 'exames': exames})


@login_required
@permission_required('laboratorio.change_exame', raise_exception=True)
def detalhar(request, pk):
    exame = Exame.objects.get(pk=pk)
    return render(request, 'laboratorio/exame/detalhar.html', {'exame': exame})


@login_required
@permission_required('laboratorio.change_exame', raise_exception=True)
def adicionar(request):
    if request.method == 'POST':
        form = ExameForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse('exame_listar'))
    else:
        form = ExameForm()
    return render(request, 'laboratorio/exame/form.html', {'form': form})


@login_required
@permission_required('laboratorio.change_exame', raise_exception=True)
def editar(request, pk):
    exame = Exame.objects.get(pk=pk)
    if request.method == 'POST':
        form = ExameForm(request.POST, instance=exame)
        if form.is_valid():
            form.save()
            return redirect(reverse('exame_listar'))
    else:
        form = ExameForm(instance=exame)
    return render(request, 'laboratorio/exame/form.html',
                  {'form': form, 'object': exame})


@login_required
@permission_required('laboratorio.delete_exame', raise_exception=True)
def apagar(request, pk):
    if request.method == 'POST':
        exame = Exame.objects.get(pk=pk)
        exame.delete()
    return redirect(reverse('exame_listar'))
