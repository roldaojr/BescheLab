# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required, permission_required
from django.db.models import Sum
from formtools.wizard.views import SessionWizardView
from ...layout.decorators import any_permission_required
from ...financeiro.forms import LancamentoForm
from ..forms import ExameMarcadoForm, EditarExameMarcadoForm
from ..models import ExameMarcado, Exame
from ..tables import ExameMarcadoTable


def gerar_lancamento(exame_marcado):
    total = 0
    descricao = []
    if isinstance(exame_marcado, ExameMarcado):
        exames = exame_marcado.exames
        pessoa_id = exame_marcado.paciente_id
    else:
        exames = exame_marcado.get('exames')
        pessoa_id = exame_marcado.get('paciente')
    for exame in exames.all():
        total += exame.valor
        descricao.append('%s' % (exame))

    return {
        'valor': float(total),
        'descricao': '\n'.join(descricao),
        'pessoa': pessoa_id,
    }


@login_required
@any_permission_required('laboratorio.change_examemarcado',
                         'laboratorio.realizar_examemarcado')
def listar(request):
    exames = ExameMarcado.objects.filter(
        ponto_coleta=request.ponto_coleta, feito=False).order_by('data')
    table = ExameMarcadoTable(exames)
    return render(request, 'laboratorio/exame_marcado/listar.html',
                  {'table': table, 'exames': exames})


@login_required
@any_permission_required('laboratorio.change_examemarcado',
                         'laboratorio.realizar_examemarcado')
def editar(request, pk):
    exame = ExameMarcado.objects.get(pk=pk)
    if request.method == 'POST':
        form = EditarExameMarcadoForm(request.POST, instance=exame)
        if form.is_valid():
            form.save()
            return redirect(reverse('exame_marcado_listar'))
    else:
        readonly_fields = []
        if not request.user.has_perm('laboratorio.change_examemarcado'):
            readonly_fields.extend(('exames', 'data'))
        if not request.user.has_perm('laboratorio.realizar_examemarcado'):
            readonly_fields.append('feito')
        form = EditarExameMarcadoForm(readonly_fields=readonly_fields,
                                      instance=exame)
    return render(request, 'laboratorio/exame_marcado/form.html',
                  {'form': form, 'object': exame})


@login_required
@permission_required('laboratorio.delete_examemarcado', raise_exception=True)
def apagar(request, pk):
    if request.method == 'POST':
        exame = ExameMarcado.objects.get(pk=pk)
        exame.delete()
    return redirect(reverse('exame_marcado_listar'))


class AdicionarWizard(SessionWizardView):
    form_list = [
        ('exame', ExameMarcadoForm),
        ('pagamento', LancamentoForm)
    ]
    template_names = {
        'exame': 'laboratorio/exame_marcado/form.html',
        'pagamento': 'lancamento/adicionar.html'
    }
    condition_dict = {'pagamento': lambda w: w.requer_pagamento}
    requer_pagamento = True

    def calcula_pagamento(self, exames_ids):
        total = Exame.objects.filter(id__in=exames_ids).aggregate(
            total=Sum('valor'))['total']
        self.requer_pagamento = total > 0
        return self.requer_pagamento

    def get_template_names(self):
        return self.template_names[self.steps.current]

    def process_step(self, form):
        if self.steps.current == 'exame':
            self.calcula_pagamento(form.data.getlist('exame-exames'))
            lancamento = gerar_lancamento(form.cleaned_data)
            self.initial_dict['pagamento'] = lancamento
        return self.get_form_step_data(form)

    def done(self, form_list, **kwargs):
        if self.requer_pagamento:
            form_exame, form_pagamento = form_list
        else:
            form_exame, = form_list
        em = form_exame.save(commit=False)
        em.ponto_coleta = self.request.ponto_coleta
        em.save()
        form_exame.save_m2m()
        if self.requer_pagamento:
            pagamento = form_pagamento.save(commit=False)
            pagamento.ponto_coleta = self.request.ponto_coleta
            pagamento.tipo = 1
            pagamento.save()
        return redirect(reverse('exame_marcado_listar'))


adicionar = AdicionarWizard.as_view()
